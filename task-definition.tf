data "aws_ecs_task_definition" "wordpress" {
  task_definition = "${aws_ecs_task_definition.wordpress.family}"
}

resource "aws_ecs_task_definition" "wordpress" {
  family = "hello_world"

  container_definitions = "${data.template_file.definition.rendered}"
}
