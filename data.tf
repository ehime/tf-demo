# Declare the data source
data "aws_availability_zones" "available" {}

data "aws_iam_policy_document" "ecs-instance-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ecs-service-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}

data "template_file" "cluster" {
  template = "${file("${path.module}/templates/cluster.tpl.sh")}"

  vars = {
    ecs_cluster = "${var.ecs_cluster}"
  }
}

data "template_file" "definition" {
  template = "${file("${path.module}/templates/definition.tpl.json")}"
}
