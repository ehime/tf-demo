# Terraform Cluster Demo
## Documentation generation
Documentation should be modified within `main.tf` and generated using [terraform-docs](https://github.com/segmentio/terraform-docs):
```bash
terraform-docs md ./ |sed '$d' >| README.md
```
## License
GPL 3.0 Licensed. See [LICENSE](https://gitlab.com/ehime/tf-demo/tree/master/LICENSE) for full details.


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| availability_zone | availability zone used for the demo, based on region | string | `<map>` | no |
| aws_access_key_id | AWS access key | string | - | yes |
| aws_secret_access_key | AWS secret access key | string | - | yes |
| desired_capacity | Desired number of instances in the cluster | string | - | yes |
| ecs_cluster | ECS cluster name | string | - | yes |
| ecs_key_pair_name | ECS key pair name | string | - | yes |
| max_instance_size | Maximum number of instances in the cluster | string | - | yes |
| min_instance_size | Minimum number of instances in the cluster | string | - | yes |
| region | AWS region | string | - | yes |
| test_network_cidr | IP addressing for Test Network | string | - | yes |
| test_public_01_cidr | Public 0.0 CIDR for externally accessible subnet | string | - | yes |
| test_public_02_cidr | Public 0.0 CIDR for externally accessible subnet | string | - | yes |
| test_vpc | VPC for Test environment | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| ecs-instance-role-name |  |
| ecs-load-balancer-name |  |
| ecs-service-role-arn |  |
| ecs-target-group-arn |  |
| region |  |
| test_public_sg_id |  |
| test_public_sn_01_id |  |
| test_public_sn_02_id |  |
| test_vpc_id |  |
