# Version constraint
terraform {
  required_version = "~> 0.11"
}

# Setup our aws provider
provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region     = "${var.region}"
}

provider "null" {}
provider "template" {}
provider "random" {}
