/**
# Terraform Cluster Demo
## Documentation generation
Documentation should be modified within `main.tf` and generated using [terraform-docs](https://github.com/segmentio/terraform-docs):
```bash
terraform-docs md ./ |sed '$d' >| README.md
```
## License
GPL 3.0 Licensed. See [LICENSE](https://gitlab.com/ehime/tf-demo/tree/master/LICENSE) for full details.
*/

